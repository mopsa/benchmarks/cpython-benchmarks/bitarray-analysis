This repository is a copy of the [bitarray library](https://github.com/ilanschnell/bitarray), using commit 66a6698b25b8739e45eca7bb2a434b37c6f3fa98.

Run `make` to build the library using `mopsa-build` and launch the analysis of the tests.
The results are written in text files by default.

Minor changes performed to analyze the library are shown in CHANGELOG.md
