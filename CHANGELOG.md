- __init__.py: no absolute import, removed Python2 compatibility code, removed *args/**kwargs in __delitem__
- bitarray.h: import stubs if MOPSA macro defined
- test_bitarray.py:
  + removed tests performing I/O (including calls to tempfile, shutil, pickle io.BytesIO, shelve, haslib modules)
  + removed Python 2.x compatibility hacks
  + disabled copy tests
  + do not explicitly pass the tests to run, just run unittest.main()
  + remove other tests where the analysis was too unpreicse
  + replace assertRaisesMessage with assertRaisesRegex (supported by mopsa)
  + currently use list rather than generators in randombitarrays
- test_util.py: removed hexdigits, os, sys dependencies.
- util.py: remove Python2 compatibility code
- examples/bloom.py: commented Python 2 compatibility code + generator comprehension moved to list comprehension
- examples/gene.py: replaced timeit with simple loop
- examples/sieve.py: commented Python 2 compatibility code
- other examples are not and have been removed
